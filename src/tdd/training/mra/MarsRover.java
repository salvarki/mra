package tdd.training.mra;

import java.util.List;

public class MarsRover {
	public static final String OBSTACLE = "X";

	private String[][] planet ;
	private int positionX;
	private int positionY;
	private char direction;

	public int getPositionX() {
		return positionX;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}

	public char getDirection() {
		return direction;
	}

	public void setDirection(char direction) {
		this.direction = direction;
	}

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		planet = new String[planetX][planetY];
		this.setPositionX(0);
		this.setPositionY(0);
		this.setDirection('N');
		
		int x;
		int y;
		
		for(int i=0; i < planetObstacles.size(); i++) {
			String obstacle = planetObstacles.get(i);
			obstacle = obstacle.replaceAll("[^0-9?!\\,]","");
			String[] coordinateObstacle = obstacle.split(",");
			x = Integer.valueOf(coordinateObstacle[0]);
			y = Integer.valueOf(coordinateObstacle[1]);
			planet[x][y] = OBSTACLE;
		}
		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		if(this.planet[x][y] == "X")return true;
		else return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		for(int i = 0; i < commandString.length(); i++) {
			String tempString = commandString;
			commandString = commandString.substring(0+i,i+1);
			if(commandString.equals("r")) {
				this.direction = 'E';
			}
			if(commandString.equals("l")) {
				this.direction = 'W';
			}
			if(commandString.equals("f")) {
				if(this.getDirection() == 'N') {
					this.positionY ++;
				}else if(this.getDirection() == 'E') {
					this.positionX ++;
				}else if(this.getDirection() == 'W') {
					this.positionX --;
				}else if(this.getDirection() == 'S') {
					this.positionY --;
				}
			}
			if(commandString.equals("b")) {
				if(this.getDirection() == 'S') {
					if(getPositionY() == planet[1].length-1) {
						setPositionY(0);
					}else {
						this.positionY ++;
					}
				}else if(this.getDirection() == 'W') {
					if(this.getPositionX() == planet[0].length-1) {
						setPositionX(0);
					}else {
						this.positionX ++;
					}
					
				}else if(this.getDirection() == 'E') {
					if(getPositionX() == 0) {
						setPositionX(Integer.valueOf(planet[0].length));
					}
					this.positionX --;
				}else if(this.getDirection() == 'N') {
					if(getPositionY() == 0) {
						setPositionY(Integer.valueOf(planet[1].length));
					}
					this.positionY --;
				}
			}
			commandString = tempString;
		}

		
		
		return commandString = "("+String.valueOf(this.positionX)+","+String.valueOf(this.positionY)+","+this.direction+")";
	
	}

}
