package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void ShouldReturnAPlanetWithObstacleInFiveFive() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);

		assertTrue(rover.planetContainsObstacleAt(5, 5));
	}
	
	@Test
	public void ShouldReturnAPlanetWithObstacleInTwoTwo() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);

		assertTrue(rover.planetContainsObstacleAt(2, 2));
	}
	
	@Test
	public void ShouldReturnAPlanetWithTwoObstacles() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,3)");
		planetObstacles.add("(4,7)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);

		assertTrue(rover.planetContainsObstacleAt(2, 3));
		assertTrue(rover.planetContainsObstacleAt(4, 7));
	}
	
	@Test
	public void RoverShouldBeAtZeroZeroNord() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,3)");
		planetObstacles.add("(4,7)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,N)", rover.executeCommand(""));
	}
	
	@Test
	public void RoverShouldBeAtZeroZeroEst() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,3)");
		planetObstacles.add("(4,7)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,E)", rover.executeCommand("r"));
	}
	
	public void RoverShouldBeAtZeroZeroWest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,3)");
		planetObstacles.add("(4,7)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		
		assertEquals("(0,0,W)", rover.executeCommand("l"));
	}
	
	@Test
	public void RoverShouldMoveForwardFromNord() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPositionX(7);
		rover.setPositionY(6);
		rover.setDirection('N');
	
		assertEquals("(7,7,N)", rover.executeCommand("f"));
	}
	
	@Test
	public void RoverShouldMoveForwardFromEast() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPositionX(7);
		rover.setPositionY(6);
		rover.setDirection('E');
	
		assertEquals("(8,6,E)", rover.executeCommand("f"));
	}
	
	@Test
	public void RoverShouldMoveForwardFromWeast() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPositionX(7);
		rover.setPositionY(6);
		rover.setDirection('W');
	
		assertEquals("(6,6,W)", rover.executeCommand("f"));
	}
	
	@Test
	public void RoverShouldMoveForwardFromSouth() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPositionX(7);
		rover.setPositionY(6);
		rover.setDirection('S');
	
		assertEquals("(7,5,S)", rover.executeCommand("f"));
	}
	
	@Test
	public void RoverShouldMoveBackwardFromEast() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPositionX(5);
		rover.setPositionY(8);
		rover.setDirection('E');
	
		assertEquals("(4,8,E)", rover.executeCommand("b"));
	}
	
	@Test
	public void RoverShouldMoveBackwardFromWeast() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPositionX(5);
		rover.setPositionY(8);
		rover.setDirection('W');
	
		assertEquals("(6,8,W)", rover.executeCommand("b"));
	}
	
	@Test
	public void RoverShouldMoveBackwardFromNord() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPositionX(5);
		rover.setPositionY(8);
		rover.setDirection('N');
	
		assertEquals("(5,7,N)", rover.executeCommand("b"));
	}
	
	@Test
	public void RoverShouldMoveBackwardFromSouth() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPositionX(5);
		rover.setPositionY(8);
		rover.setDirection('S');
	
		assertEquals("(5,9,S)", rover.executeCommand("b"));
	}
	
	@Test
	public void RoverShouldMoveSequentially() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
	
		assertEquals("(2,2,E)", rover.executeCommand("ffrff"));
	}
	
	@Test
	public void RoverShouldMoveInOppositeEdgeSouthWestToNorthWest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);

		assertEquals("(0,9,N)", rover.executeCommand("b"));
	}
	
	@Test
	public void RoverShouldMoveInOppositeEdgeSouthEstToNorthEest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPositionX(9);
		rover.setPositionY(0);
		rover.setDirection('N');
		
		assertEquals("(9,9,N)", rover.executeCommand("b"));
	}
	
	@Test
	public void RoverShouldMoveInOppositeEdgeSouthEstToSouthEest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setDirection('E');
		
		assertEquals("(9,0,E)", rover.executeCommand("b"));
	}
	
	@Test
	public void RoverShouldMoveInOppositeEdgeSouthWestToSouthEest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setDirection('W');
		rover.setPositionX(9);
		
		assertEquals("(0,0,W)", rover.executeCommand("b"));
	}
	
	@Test
	public void RoverShouldMoveInOppositeEdgeNorthWestToSouthWest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setDirection('S');
		rover.setPositionY(9);
		
		assertEquals("(0,0,S)", rover.executeCommand("b"));
	}
	
	@Test
	public void RoverShouldMoveInOppositeEdgeNorthEstToSouthEst() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setDirection('S');
		rover.setPositionX(9);
		rover.setPositionY(9);
		
		assertEquals("(9,0,S)", rover.executeCommand("b"));
	}
	
	@Test
	public void RoverShouldMoveInOppositeEdgeNorthEstToNorthEst() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setDirection('W');
		rover.setPositionX(9);
		rover.setPositionY(9);
		
		assertEquals("(0,9,W)", rover.executeCommand("b"));
	}
	
	

	


}
